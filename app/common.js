/**
 * Created by strrife on 5/29/16.
 */
module.exports = function (groupped){
    var avg = groupped.reduce((sum, item) => sum + item, 0) / groupped.length;
    var disp = groupped.reduce((sum, item) => sum + Math.pow(item - avg, 2), 0) / groupped.length
    return [avg, disp]
};