var fs = require('fs');
var stat = require('./common');

function randomString(len){
    len = len || 5;
    var text = "";
    var possible = "ABCDEFG";
    for( var i=0; i < len; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function randomNumber(){
    return Math.floor(Math.random() * 900 + 100) / 10;
}

var array = Array(1000000)
    .fill(0)
    .map(e => [randomString(), randomNumber()]);

var string = array.map(e => e[0] + ' ' + e[1]).join('\n');
fs.writeFileSync('test.txt', string);

var groupped = array.reduce((memo, cur) => {
    memo[cur[0]] || (memo[cur[0]] = []);
    memo[cur[0]].push(cur[1]);
    return memo;
}, {});

var result = Object.keys(groupped).map(k => {
    var res = stat(groupped[k]);
    return [k, res[0].toFixed(5), res[1].toFixed(5)];
}).map(e => e.join(' ')).sort().join('\n');

fs.writeFileSync('test-result.txt', result);