#!/usr/bin/env python
import sys

eventCountArray = {}

# Input is from STDIN
for line in sys.stdin:
    # Remove leading and trailing whitespace
    line = line.strip()

    # Parse the input from the mapper
    event, number = line.split('\t', 1)

    # Cast count to int
    try:
        number = float(number)
    except ValueError:
        continue

    # Compute event count
    try:
        eventCountArray[event][0] += 1
        eventCountArray[event][1] += number ** 2
        eventCountArray[event][2] += number
    except:
        eventCountArray[event] = [1, number ** 2, number]
#
# # Write the results (unsorted) to stdout
for event in eventCountArray.keys():
    e = eventCountArray[event]
    avg = e[2] / e[0]
    disp = (e[1] - e[0] * (avg ** 2)) / e[0]

    print '%s\t%s\t%s' % (event, avg, disp)

