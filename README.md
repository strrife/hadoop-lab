To generate input:

` cd app && node geenrator.js`

To start hadoop in docker:

` docker run -it  -v /Users/strrife/Projects/docker-hadoop:/var/nodeapp sequenceiq/hadoop-docker:2.7.1  /etc/bootstrap.sh -bash`

To start a job:

` ./test.sh `

Based on this:

http://www.michael-noll.com/tutorials/writing-an-hadoop-mapreduce-program-in-python/